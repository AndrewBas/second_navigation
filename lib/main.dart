import 'package:flutter/material.dart';
import 'package:second_navigate_app/main_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
     /* initialRoute: '/',
      routes: {
        '/': (ctx) => MainScreen(),
        MainScreen.routeName: (ctx) => MainScreen(),
        ListScreen.routeName: (ctx) => ListScreen(),*/
    // },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key,}) : super(key: key);


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: MainScreen(),


    );
  }
}
