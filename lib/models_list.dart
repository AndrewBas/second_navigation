import 'package:flutter/material.dart';
import 'package:second_navigate_app/model.dart';

class ModelsList extends StatelessWidget {



  final List<Model> modelList = [
    Model(title: 'default name', url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4R_Gfs6C_JymOJEWEtncnuw3OdsMJetvceg&usqp=CAU'),
    Model(title: 'default name1', url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoFQBxSGaX6V2Q9rqTxFwAyVOj4xOzA7ZAHw&usqp=CAU'),
    Model(title: 'default name2', url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8DaoGSyP6fo2EqoCvOVMZtiPx7xSg-TiiDw&usqp=CAU'),
    Model(title: 'default name3', url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPpuao3a0xRRSsSK-REyWKEI3X404umW_F3A&usqp=CAU'),
    Model(title: 'default name4', url: 'https://images.alphacoders.com/239/239201.jpg'),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(itemBuilder: (ctx, id){
        return Card(
          child: Row(
            children: [

              Container(
                width: 220,
                height: 220,

                child: Image(image: NetworkImage('${modelList[id].url}'), fit: BoxFit.contain,),
              ),
            ],
          ),
        );
      },
        itemCount: modelList.length,
      ),
    );
  }
}
