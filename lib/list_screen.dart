import 'package:flutter/material.dart';
import 'package:second_navigate_app/main_screen.dart';
import 'package:second_navigate_app/models_list.dart';

class SecondScreen extends StatelessWidget {

  void gotoMainScreen(BuildContext ctx){

    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return MainScreen();
    }
    )
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('List Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            Container(height:50,
                width: double.infinity,child: FlatButton(onPressed: () => gotoMainScreen(context), child: Text('goToMainScreen'))),
            Container(
                height: 480,child: ModelsList()),
          ],
        ),
      ),
    );
  }
}