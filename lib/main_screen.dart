import 'package:flutter/material.dart';
import 'package:second_navigate_app/list_screen.dart';

class MainScreen extends StatefulWidget {

  static const routeName = '/main-screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }
  void _dincrementCounter() {
    setState(() {
      _counter--;
    });
  }

  void gotoListScreen(BuildContext ctx){

    Navigator.of(ctx).push(MaterialPageRoute(builder: (_){
      return SecondScreen();
    }
    )
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            FlatButton(onPressed: () => gotoListScreen(context)
                , child: Text('goToSecondScreen')),
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: FloatingActionButton(
              heroTag: 'fab1',
              onPressed: _incrementCounter,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: FloatingActionButton(
              heroTag: 'fab2',
              onPressed: _dincrementCounter,
              tooltip: 'Dincrement',
              child: Icon(Icons.exposure_minus_1),
            ),
          ),
        ],
      ),
    );
  }
}
